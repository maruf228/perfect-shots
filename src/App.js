import React, { useState } from 'react';
import './App.css';
import Title from './components/Title';
import UploadFiles from './components/UploadFiles';
import ImageGallery from './components/ImageGallery';
import Modal from './components/Modal';

function App() {
  const [selectedImage, setSelectedImage] = useState(null)

  return (
    <div className="App">
      <Title />
      <UploadFiles />
      <ImageGallery setSelectedImage={setSelectedImage} />
      {
        selectedImage && <Modal selectedImage={selectedImage} setSelectedImage={setSelectedImage} />
      }
    </div>
  );
}

export default App;
