import React from 'react'
import './Title.css'

function Title() {
  return (
    <div>
      <h2 className="title">Perfect Shots</h2>

      <div className="heading">
        <h1>My Photo Gallery</h1>
      </div>
    </div>
  )
}

export default Title
