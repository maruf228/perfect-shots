import React from 'react';
import { useFirestore } from '../hooks/useFirestore';
import './image-gallery.css';
import { motion } from 'framer-motion'

function ImageGallery({ setSelectedImage }) {
  const { docs } = useFirestore('images');
  console.log(docs);

  return (
    <div className="image-container">
      { docs && docs.map(doc => (
          <motion.div className="image-wrapper" key={doc.id}
            layout
            whileHover={{ opacity: 1 }}
            onClick={() => setSelectedImage(doc.url)}
          >
            <motion.img src={doc.url} alt="image" 
              initial={{ opacity: 0 }}
              animate={{ opacity: 1 }}
              transition={{ delay: 1 }}

            />
          </motion.div>
        ))
      }
    </div>
  )
}

export default ImageGallery
