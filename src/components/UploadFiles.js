import React, {useState} from 'react'
import ProgressBar from './ProgressBar';
import './upload-files.css';

function UploadFiles() {
  const [file, setFile] = useState(null);
  const [error, setError] = useState(null);

  const types = ['image/png', 'image/jpeg'];

  const changeHandler = (event) => {
    let selected = event.target.files[0];

    if(selected && types.includes(selected.type)) {
      setFile(selected);
      setError('');
    } else {
      setFile(null)
      setError('Please select an image file (jpeg, jpg or png)');
    }
  }
  
  return (
    <div>
      <label className="label">
        <input className="input-box" type="file" onChange={changeHandler} />
        <div className="add-button">+</div>
      </label>
      
      <div>
        {error && <div>{error}</div>}
        {file && <div className="file-name">{file.name}</div>}
        {file && <ProgressBar file={file} setFile={setFile} />}
      </div>
    </div>
  )
}

export default UploadFiles
