import React from 'react';
import './modal.css';
import { motion } from 'framer-motion';

function Modal({ selectedImage, setSelectedImage }) {
  const hideModal = (event) => {
    if(event.target.classList.contains('backdrop')) {
      setSelectedImage(null)
    }
  }

  return (
    <div className="backdrop" onClick={hideModal}>
      <motion.img src={selectedImage} alt="selected Image" className="image" 
        initial={{ y: '-100vh' }}
        animate={{ y: 0 }}
      />
    </div>
  )
}

export default Modal
