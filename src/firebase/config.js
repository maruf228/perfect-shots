import * as firebase from 'firebase/app';
import 'firebase/storage';
import 'firebase/firestore';

// Your web app's Firebase configuration
var firebaseConfig = {
  apiKey: "AIzaSyBEfuEkClzwvOSolrAF7ZsvuCRIxrY8w_Y",
  authDomain: "react-perfect-shots.firebaseapp.com",
  databaseURL: "https://react-perfect-shots.firebaseio.com",
  projectId: "react-perfect-shots",
  storageBucket: "react-perfect-shots.appspot.com",
  messagingSenderId: "955610304854",
  appId: "1:955610304854:web:81a4cb02425a8fbf4c46f7"
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);

const projectStorage = firebase.storage();
const projectFirestore = firebase.firestore();
const timestamp = firebase.firestore.FieldValue.serverTimestamp;

export { projectStorage, projectFirestore, timestamp };